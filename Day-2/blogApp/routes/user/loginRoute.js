const express = require("express");
const bcrypt = require("bcryptjs");
const { body, validationResult } = require("express-validator");
const router = express.Router();
const jwt = require("jsonwebtoken");
const User = require("../../models/userSchema");

const JWT_SECRET = "helloworld";

//Login Route
router.post(
  "/signin",
  [
    body("email", "Enter a valid email").isEmail(),
    body("password", "Password cannot be blank").exists(),
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const { email, password } = req.body;

    try {
      const user = await User.findOne({ email });

      if (!user) throw new Error("Please enter login with correct credentials");

      const passwordCompare = await bcrypt.compare(password, user.password);

      if (!passwordCompare)
        return res
          .status(400)
          .json({ error: "Please try to login with correct credentials" });

      const data = {
        user: {
          id: user.id,
        },
      };

      const authtoken = jwt.sign(data, JWT_SECRET);
      res.json({ authtoken });
    } catch (error) {
      return res.status(404).send({
        error: true,
        message: error.message || "Internal server error",
      });
    }
  }
);

module.exports = router;
