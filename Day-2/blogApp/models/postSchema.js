const mongoose = require("mongoose");
const { Schema } = mongoose;

const blogSchema = new Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },

  title: String,
  description: String,
  image: String,
});

module.exports = mongoose.model("BlogPost", blogSchema);
