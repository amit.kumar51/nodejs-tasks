const axios = require('axios');
const xml2js = require('xml2js');

// Function to make a SOAP request and parse the response
async function soapRequest(url, headers, body) {
  const response = await axios.post(url, body, { headers });
  const xml = response.data;
  const result = await xml2js.parseStringPromise(xml, { explicitArray: false });
  return result;
}

const headers = {
    'Content-Type': 'text/xml; charset=utf-8',
};

const body = `<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <NumberToWords xmlns="http://www.dataaccess.com/webservicesserver/">
      <ubiNum>500</ubiNum>
    </NumberToWords>
  </soap:Body>
</soap:Envelope>`;




soapRequest(' https://www.dataaccess.com/webservicesserver/NumberConversion.wso', headers, body)
  .then(response => {
    console.log((response['soap:Envelope']['soap:Body']['m:NumberToWordsResponse']['m:NumberToWordsResult']['CelsiusToFahrenheitResult']))
  })
  .catch(error => console.error(error));

module.exports = soapRequest;