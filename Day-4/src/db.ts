import mongoose from "mongoose";

const dbRun = () => {
    try {
        mongoose.connect('mongodb://localhost:27017/operators');
        console.log("Data base connected successfully");
    } catch (error) {
        console.log("DB Connection Error");
    }
}

export default dbRun;