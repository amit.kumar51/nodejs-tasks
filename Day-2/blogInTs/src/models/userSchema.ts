import { IPerson } from "../interfaces";
const mongoose = require("mongoose");
const { Schema } = mongoose;

const userSchema = new Schema({
  name: {
    type: String,
    required: true,
  },

  username: {
    type: String,
    required: true,
  },

  email: {
    type: String,
    required: true,
    unique: [true, "Email should be unique"],
  },

  password: {
    type: String,
    required: true,
  },

  phone: {
    type: String,
  },

  token: {
    type: String,
    default: "",
  },
});

export default mongoose.model("User", userSchema);
