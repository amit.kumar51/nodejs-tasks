import { v2 as cloudinary } from "cloudinary";
import { CloudinaryStorage } from "multer-storage-cloudinary";
import multer from "multer";

cloudinary.config({
  cloud_name: process.env.CLOUD_NAME,
  api_key: process.env.API_KEY,
  api_secret: process.env.API_SECRET,
});

const myParams: { folder: string; allowed_formats: string[] } = {
  folder: "uploads",
  allowed_formats: ["jpg", "png", "jpeg"],
};

const storage = new CloudinaryStorage({
  cloudinary: cloudinary,
  params: myParams,
});

export default multer({ storage: storage });
