const { body, validationResult } = require("express-validator");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const User = require("../../models/userSchema");

const JWT_SECRET = "helloworld";

const signUp = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }

  try {
    let user = await User.findOne({ email: req.body.email });

    if (user) {
      return res
        .status(400)
        .json({ error: "Sorry a user with this email already exists" });
    }

    const salt = bcrypt.genSaltSync(10);
    const secPass = await bcrypt.hashSync(req.body.password, salt);

    user = await User.create({
      name: req.body.name,
      username: req.body.username,
      email: req.body.email,
      password: secPass,
      phone: req.body.phone,
    });

    const data = {
      user: {
        id: user.id,
      },
    };
    // JWT Athentication
    const authtoken = jwt.sign(data, JWT_SECRET);
    res.json({ authtoken });
  } catch (error) {
    console.error(error.message);
    res.status(500).send("Some Error occured");
  }
};

const signIn = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }

  const { email, password } = req.body;

  try {
    const user = await User.findOne({ email });

    if (!user) throw new Error("Please enter login with correct credentials");

    const passwordCompare = await bcrypt.compare(password, user.password);

    if (!passwordCompare)
      return res
        .status(400)
        .json({ error: "Please try to login with correct credentials" });

    const data = {
      user: {
        id: user.id,
      },
    };

    const authtoken = jwt.sign(data, JWT_SECRET);
    res.json({ authtoken });
  } catch (error) {
    return res.status(404).send({
      error: true,
      message: error.message || "Internal server error",
    });
  }
};

const forgotPassword = (req, res) => {
  res.send("Welcome to forgot password page");
  console.log(req.body);
};
