import mongoose from "mongoose";
import { IComment } from "../interfaces";
const { Schema } = mongoose;

const commentSchema = new Schema<IComment>({
  postId: {
    type: Schema.Types.ObjectId,
    ref: "Post",
  },

  text: {
    type: String,
    required: true,
  },
});

export default mongoose.model<IComment>("Comments", commentSchema);
