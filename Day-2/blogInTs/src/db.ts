const { default: mongoose } = require("mongoose");

// Connection URI
const uri: String = "mongodb://localhost:27017/blogTs";

export const dbRun = (): void => {
  try {
    mongoose.connect(uri);
    console.log("DB connected Successfully");
  } catch (error) {
    console.log("Some connection Error");
  }
};
