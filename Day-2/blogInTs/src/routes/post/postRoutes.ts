import express, { Router } from "express";
import upload from "../../middleware/imageUpload";
import {
  createPost,
  editPost,
  fetchAllPosts,
  likePost,
  postComment,
  unlikePost,
} from "../../controllers/postController";
import validUser from "../../middleware/validUser";
import { validComment, validPost } from "../../middleware/validatorMiddleware";
const router: Router = express.Router();

router.post("/create", validUser, upload.single("image"), createPost);
router.put("/edit/:id", validUser, editPost);
router.get("/allposts", validUser, fetchAllPosts);
router.post("/comment/:id", validComment, validUser, postComment);
router.put("/like/:id", validUser, likePost);
router.put("/unlike/:id", validUser, unlikePost);

export default router;
