import express from "express";
import dbRun from "./db";
import movie from "./model/users";
import { addUser, showData } from "./controllers/userController";

const app = express();
app.use(express.json());

app.get("/show", showData);
app.post(`/add`, addUser);

dbRun();

app.listen(5000, () => {
  console.log("Server is connected at 5000 port");
});
