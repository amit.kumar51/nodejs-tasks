import mongoose from "mongoose";
import { IPost } from "../interfaces";
const { Schema } = mongoose;

const blogSchema = new Schema<IPost>({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
  },

  title: String,
  description: String,
  image: String,

  likes: {
    type: [mongoose.Schema.Types.ObjectId],
    ref: "User",
    default: [],
  },

  unlikes: {
    type: [mongoose.Schema.Types.ObjectId],
    ref: "User",
    default: [],
  },
});

export default mongoose.model<IPost>("BlogPost", blogSchema);
