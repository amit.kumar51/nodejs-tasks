const express = require("express");
const app = express();

const port = 5000;

app.get("/user", (req, res) => {
  res.send("Hello World");
});

app.post("/", (req, res) => {
  res.send("Me parminder hun");
});

app.listen(port, () => {
  console.log(`App is running on port ${port}`);
});
