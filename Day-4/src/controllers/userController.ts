import { Response, Request } from "express";
import user from "../model/users";

export const showData = async (req: Request, res: Response) => {
  //Comparision Operators
  const user1 = await user.find({ age: { $eq: 22 } });
  //console.log(user1);

  const users1 = await user.find({ age: { $gt: 22 } });
  //console.log(users1)

  const users2 = await user.find({ age: { $gte: 22 } });
  //console.log(users2)

  const users3 = await user.find({ age: { $in: [22, 25] } }, { _id: 0 });
  //console.log(users3)

  const users4 = await user.find({ age: { $lt: 22 } });
  //console.log(users4);

  const users5 = await user.find({ age: { $lte: 22 } });
  //console.log(users5);

  const users6 = await user.find({ age: { $ne: 22 } });
  //console.log(users6);

  const users7 = await user.find({ age: { $nin: [22, 25] } }, { _id: 0 });
  //console.log(users7);

  // Logical Operators
  const users8 = await user.find({ age: { $ne: 22, $exists: true } });
  //res.send(users8)

  const users9 = await user.find({ age: { $not: { $gt: 23 } } });
  // res.send(users9)

  const users10 = await user.find({ $nor: [{ age: 22 }, { age: 23 }] });
  // res.send(users10);

  const users11 = await user.find({ $or: [{ age: 12 }, { age: 23 }] });
  // res.send(users11);

  // Array Query Operations
  const users12 = await user.find({ address: { $all: ["Kharar"] } });
  //res.send(users12);

  const users13 = await user.find({
    phone: { $elemMatch: { eq: "9876543210" } },
  });
  res.send(users13);

  const users14 = await user.find({ phone: { $size: 4 } });
  //res.send(users14);

  // Update Query Operations
  const userNew = await user.findOne({ email: "amit@gmail.com" });
  const id = userNew?._id;

  const user15 = await user.updateOne({ _id: id }, { $set: { age: 30 } });
  //res.send(user15);

  const user16 = await user.updateOne({ _id: id }, { $inc: { age: 10 } });
  //res.send(user16);

  const user17 = await user.updateOne({ _id: id }, { $set: { age: 10 } });
  // res.send(user17)

  //const user18 = await user.updateOne({ _id: id }, { $unset: { email: "" } });
  //res.send(user18);

  // const user19 = await user.updateOne(
  //   { _id: id },
  //   { $push: { phone: "8988154161" } }
  // );
  // res.send(user19);

  // const user20 = await user.updateOne(
  //   { _id: id },
  //   { $pull: { phone: "8988154161" } }
  // );
  // res.send(user20);
};

export const addUser = (req: Request, res: Response) => {
  user.create({
    firstname: req.body.firstname,
    lastname: req.body.lastname,
    email: req.body.email,
    age: req.body.age,
    education: req.body.education,
    address: req.body.address,
    phone: req.body.phone,
  });
  res.send(req.body);
};
