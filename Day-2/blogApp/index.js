const express = require("express");
const { dbRun } = require("./db");
const app = express();
app.use(express.json());
const port = 5000;

// User routes
app.use("/user", require("./routes/user/loginRoute"));
app.use("/user", require("./routes/user/signUpRoute"));
app.use("/user", require("./routes/user/forgotPasswordRoute"));

// Post routes
app.use("/post", require("./routes/post/createPost"));
app.use("/post", require("./routes/post/editPost"));
app.use("/post", require("./routes/post/fetchAllPosts"));

dbRun();

app.listen(port, () => {
  console.log(`Server is running  on port ${port}`);
});
