const soapRequest = require('./firstApi')

const headers = {
    'Content-Type': ' application/soap+xml; charset=utf-8',
};

const body = `<?xml version="1.0" encoding="utf-8"?>
<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
  <soap12:Body>
    <CelsiusToFahrenheit xmlns="https://www.w3schools.com/xml/">
      <Celsius>20</Celsius>
    </CelsiusToFahrenheit>
  </soap12:Body>
</soap12:Envelope>

`;

soapRequest('https://www.w3schools.com/xml/tempconvert.asmx', headers, body)
  .then(response => {
    console.log((response['soap:Envelope']['soap:Body']['CelsiusToFahrenheitResponse']))
  })
  .catch(error => console.error(error));