import { body } from "express-validator";

export const validateSignUp = [
  body("name", "Name should atleast have 3 characters").isLength({ min: 3 }),
  body("username", "Username should not be empty").isLength({ min: 2 }),
  body("email", "Enter valid mail").isEmail(),
  body("password", "Password should have atleast 5 characters").isLength({
    min: 5,
  }),
  body("phone", "Enter valid phone number").isLength({ min: 10 }),
];

export const validateSignIn = [
  body("email", "Enter valid email address").isEmail(),
  body("password", "Password should contain atleast 5 characters").isLength({
    min: 5,
  }),
];

export const validPost = [
  body("title", "Enter atleast 4 characters").isLength({ min: 4 }),
  body("description", "Enter atleast 8 characters").isLength({ min: 6 }),
];

export const validComment = [
  body("text", "Enter atleast 2 charcters").isLength({ min: 2 }),
];
