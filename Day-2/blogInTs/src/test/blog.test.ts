import chaiHttp from "chai-http";
import chai, { expect } from "chai";
import { sign } from "jsonwebtoken";
import { app } from "../index";
import { response } from "express";
import { dbRun } from "../db";
import { request } from "http";

chai.use(chaiHttp);

describe("Post API", () => {
  before(() => {
    dbRun();
  });

  const SECRETKEY = process.env.SECRETKEY;
  const authToken =
    "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoiNjQwZjEwMmYyOWRiYzgxZDVjNzk1MzZjIn0sImlhdCI6MTY3ODcwODgwNn0.LKgFO_ZNukXUGyAMzxdRyzD9yInTrRgmuY2GSakjZs4";

  const post = {
    title: "My first Post",
    description: "This is description of my first Post",
    image: "imgstring",
  };

  // Create post
  it("Create post on valid data", async () => {
    const res = await chai
      .request(app)
      .post("/post/create")
      .set("auth-token", authToken)
      .send(post);
    expect(200);
  });

  // Fetch all posts

  it("Fetch all posts with valid credentials", async () => {
    const res = await chai
      .request(app)
      .get("/post/allposts")
      .set("auth-token", authToken);

    expect(res).to.have.status(200);
  });

  it("Fetch all posts with invalid credential", async () => {
    const res = await chai.request(app).get("/post/allposts");

    expect(res).to.have.status(401);
  });
});
