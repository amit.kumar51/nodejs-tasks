const request = require('request')
const cheerio = require('cheerio') 

request('https://chandigarhmetro.com/list-government-model-primary-sr-sec-schools-chandigarh-fee-structure/', cb);

function cb(error, response, html){
    if(error){
        console.error('error', error);
    } else {
        handlehtml(html);
    }
}

function handlehtml(html){
    let selTool = cheerio.load(html);
    let paragraphs = selTool(".entry-content p");
    
    for(let i=4; i<paragraphs.length-7; i++){
        let newEle = selTool(paragraphs[i]);
        console.log(newEle.text())
    }
}