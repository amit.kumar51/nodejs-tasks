import mongoose from "mongoose";
const Schema = mongoose.Schema;

const userSchema = new Schema({
  firstname: String,
  lastname: String,
  email: String,
  age: Number,
  education: String,
  address: String,
  phone: {
    type: [String],
    default: [],
  },
  items: {
    type: String,
  },
});

export default mongoose.model("Users", userSchema);
