const soapRequest = require('./firstApi')

const headers = {
    'Content-Type': 'text/xml; charset=utf-8',
};

const body = `<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <CapitalCity xmlns="http://www.oorsprong.org/websamples.countryinfo">
      <sCountryISOCode>US</sCountryISOCode>
    </CapitalCity>
  </soap:Body>
</soap:Envelope>
`;

soapRequest('http://webservices.oorsprong.org/websamples.countryinfo/CountryInfoService.wso', headers, body)
  .then(response => {
    console.log((response['soap:Envelope']['soap:Body']['m:CapitalCityResponse']['m:CapitalCityResult']))
  })
  .catch(error => console.error(error));