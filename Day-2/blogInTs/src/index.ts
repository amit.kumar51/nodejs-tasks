import express, { Application } from "express";
import dotenv from "dotenv";
dotenv.config();

import { dbRun } from "./db";
import userRoutes from "./routes/user/userRoutes";
import postRoutes from "./routes/post/postRoutes";

export const app: Application = express();
const port = process.env.PORT || 5000;
app.use(express.json());

app.use("/user", userRoutes);
app.use("/post", postRoutes);

dbRun();

app.listen(port, () => {
  console.log(`The application is listening on port ${port}`);
});
