import express, { Router } from "express";
import {
  validateSignUp,
  validateSignIn,
} from "../../middleware/validatorMiddleware";
import {
  forgotPassword,
  resetPassword,
  signin,
  signup,
} from "../../controllers/userController";

const router: Router = express.Router();

router.post("/signup", validateSignUp, signup);
router.post("/signin", validateSignIn, signin);
router.post("/forgot-password", forgotPassword);
router.post("/reset-password", resetPassword);

export default router;
