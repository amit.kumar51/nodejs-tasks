const express = require("express");
const router = express.Router();
const { body, validationResult } = require("express-validator");
const validUser = require("../../middleware/validUser");
const Post = require("../../models/postSchema");

router.post(
  "/create",
  validUser,
  [
    body("title", "Enter a valid title").isLength({ min: 3 }),
    body("description", "Description must be atleast 5 characters").isLength({
      min: 5,
    }),
  ],
  async (req, res) => {}
);

module.exports = router;
