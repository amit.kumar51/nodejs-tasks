import { Request, Response } from "express";
import { validationResult } from "express-validator";
import bcrypt from "bcryptjs";
import jwt from "jsonwebtoken";
import randomstring from "randomstring";
import nodemailer from "nodemailer";
import User from "../models/userSchema";

const JWT_SECRET: any = process.env.SECRETKEY;

const sendresetPasswordMail = async (
  name: String,
  email: String,
  token: String
) => {
  try {
    const transporter = nodemailer.createTransport({
      host: "smtp.ethereal.email",
      port: 587,
      auth: {
        user: "burley1@ethereal.email",
        pass: "rzH4ukrPXteM9JbRfS",
      },
    });

    const mailOptions: any = {
      from: "Amit kumar <amit@gmail.com",
      to: email,
      subject: "For Reset Password",
      html: `<p>Hii ${name}, Please copy the link and <a href="http://localhost:3000/user/reset-password?token=${token}">reset your password</a></p>`,
    };

    transporter.sendMail(mailOptions, (error, info) => {
      if (error) {
        console.log(error);
      } else {
        console.log("Mail has been sent", info.response);
      }
    });
  } catch (error: any) {
    console.log(error);
  }
};

// SignUp Controller
export const signup = async (req: Request, res: Response) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }
  try {
    let user = await User.findOne({ email: req.body.email });

    if (user) {
      return res
        .status(400)
        .json({ error: "Sorry a user with this email already exists" });
    }

    const salt = bcrypt.genSaltSync(10);
    const secPass = await bcrypt.hashSync(req.body.password, salt);

    user = await User.create({
      name: req.body.name,
      username: req.body.username,
      email: req.body.email,
      password: secPass,
      phone: req.body.phone,
    });

    const data = {
      user: {
        id: user.id,
      },
    };
    // JWT Athentication
    const authtoken = jwt.sign(data, JWT_SECRET);
    res.status(200).send("User created");
  } catch (error: any) {
    res.status(500).send({ error: error.message || "Internal Server Error" });
  }
};

// SignIn Controller
export const signin = async (req: Request, res: Response) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }

  const { email, password } = req.body;

  try {
    const user = await User.findOne({ email });

    if (!user) throw new Error("Please enter login with correct credentials");

    const passwordCompare = await bcrypt.compare(password, user.password);

    if (!passwordCompare)
      return res
        .status(400)
        .json({ error: "Please try to login with correct credentials" });

    const data = {
      user: {
        id: user.id,
      },
    };

    const authtoken = jwt.sign(data, JWT_SECRET);
    res.json({ authtoken });
  } catch (error: any) {
    return res.status(404).send({
      error: true,
      message: error.message || "Internal server error",
    });
  }
};

// Forgot Password
export const forgotPassword = async (req: Request, res: Response) => {
  try {
    const email = req.body.email;
    const userData = await User.findOne({ email: req.body.email });

    if (!userData) throw Error("User with this email do not exists");

    const randomString = randomstring.generate();
    const data = await User.updateOne(
      { email: email },
      { $set: { token: randomString } }
    );
    sendresetPasswordMail(userData.name, userData.email, randomString);

    res
      .status(200)
      .send({ success: true, msg: "Please Check your inbox of mail" });
  } catch (error: any) {
    res.send(400).send({ success: false, msg: error.message });
  }
};

export const resetPassword = async (req: Request, res: Response) => {
  try {
    const token = req.query.token;
    console.log(token);
    const tokenData = await User.findOne({ token: token });
    console.log(tokenData);
    if (!tokenData) throw Error("Link expired");

    const password = req.body.password;
    const salt = bcrypt.genSaltSync(10);
    const newPassword = await bcrypt.hashSync(password, salt);

    const userData = await User.findByIdAndUpdate(
      { _id: tokenData._id },
      { $set: { password: newPassword, token: "" } },
      { new: true }
    );
    res
      .send(200)
      .send({ success: true, msg: "User password reset", data: userData });
  } catch (error: any) {
    res.send(400).send({ success: false, msg: error.message });
  }
};
