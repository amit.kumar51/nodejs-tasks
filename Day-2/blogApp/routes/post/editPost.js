const express = require("express");
const router = express.Router();
const Post = require("../../models/postSchema");
const validUser = require("../../middleware/validUser");

//Login Route
router.put("/edit/:id",validUser, async (req, res) => {
  const {title, description, image} = req.body;

  const newPost = {};
  if(title) newPost.title = title;
  if(description) newPost.description = description;
  if(image) newPost.image = image;

  let post = await Post.findById(req.params.id);
  if(!post) return res.status(404).send("User not found");

  if(post.user.toString() !== req.user.id) return res.status(401).send("Not Allowed");

  post = await Post.findByIdAndUpdate(req.params.id, {$set: newPost}, {new:true});
  res.json({post})
});

module.exports = router;
