const express = require("express");
const router = express.Router();
const validUser = require("../../middleware/validUser");
const Posts = require("../../models/postSchema");

router.get("/allposts", validUser, async (req, res) => {
  try {
    const posts = await Posts.find({ user: req.user.id });
    res.json(posts)
  } catch (error) {
    return res.status(500).send({error: true, error: error?.message || "Internal Server Error" });
  }
});

module.exports = router;
