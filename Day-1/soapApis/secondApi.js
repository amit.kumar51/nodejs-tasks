const soapRequest = require('./firstApi')

const headers = {
    'Content-Type': 'text/xml; charset=utf-8',
};

const body = `<?xml version="1.0" encoding="utf-8"?>
<soap12:Envelope xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
  <soap12:Body>
    <ListOfCountryNamesByName xmlns="http://www.oorsprong.org/websamples.countryinfo">
    </ListOfCountryNamesByName>
  </soap12:Body>
</soap12:Envelope>
`;

soapRequest(' http://webservices.oorsprong.org/websamples.countryinfo/CountryInfoService.wso', headers, body)
  .then(response => {
    console.log((response['soap:Envelope']['soap:Body']['m:ListOfCountryNamesByNameResponse']['m:ListOfCountryNamesByNameResult']['m:tCountryCodeAndName']))
  })
  .catch(error => console.error(error));