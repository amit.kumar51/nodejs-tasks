const { default: mongoose } = require("mongoose");

// Connection URI
const uri = "mongodb://localhost:27017/blog";

const dbRun = () => {
  try {
    mongoose.connect(uri);
    console.log("DB connected Successfully");
  } catch (error) {
    console.log("Some connection Error");
  }
};

module.exports = { dbRun };
