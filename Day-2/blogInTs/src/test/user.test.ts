import chai, { expect } from "chai";
import chaiHttp from "chai-http";
import { faker } from "@faker-js/faker";
import { dbRun } from "../db";
import { app } from "../index";
import request from "supertest";
import userSchema from "../models/userSchema";
import { response } from "express";

chai.use(chaiHttp);

// describe("User SingIn/SignUp", () => {
//   before(async () => {
//     dbRun();
//   });

//   it("Signup", async () => {
//     const userData = {
//       name: faker.name.fullName(),
//       username: faker.internet.userName(),
//       email: faker.internet.email(),
//       password: faker.internet.password(),
//       phone: faker.phone.number(),
//     };

//     const newUser = new userSchema(userData);
//     const savedUser = await newUser.save();
//     expect(savedUser.email).to.equal(userData.email);
//   });
// });

describe("Sign-up API", () => {
  before(async () => {
    dbRun();
  });
  it("Should create a new user with valid data", async (done) => {
    chai
      .request(app)
      .post("/signup")
      .send({
        name: faker.name.fullName(),
        username: faker.internet.userName(),
        email: faker.internet.email(),
        password: faker.internet.password(),
        phone: faker.phone.number(),
      })
      .end((err, res) => {
        chai.expect(200).to.equal(200);
      });
    done();
  });
});

describe("SignIn API", () => {
  before(() => {
    dbRun();
  });

  it("should return a token when given valid credentials", () => {
    chai.request(app).post("/signin").send({
      email: "ritu@gmail.com",
      password: "12345",
    });
    chai.expect(200);
  });

  it("should return an error when given invalid credentials", async () => {
    const res = await request(app)
      .post("/singin")
      .send({
        email: faker.internet.email(),
        password: faker.phone.number,
      })
      .expect(404);
  });
});
