import mongoose from "mongoose";

export interface IPerson {
  name: string;
  username: string;
  email: string;
  password: string;
  phone: string;
  token: string;
}

export interface IPost {
  user?: mongoose.Schema.Types.ObjectId;
  title: String;
  description: String;
  image: String;
  likes?: [mongoose.Schema.Types.ObjectId];
  unlikes?: [mongoose.Schema.Types.ObjectId];
}

export interface IComment {
  postId: String;
  text: String;
}
