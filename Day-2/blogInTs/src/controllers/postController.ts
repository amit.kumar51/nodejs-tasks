import express, {
  Errback,
  ErrorRequestHandler,
  Request,
  Response,
} from "express";
import cloudinary from "cloudinary";
import { validationResult } from "express-validator";
import Post from "../models/postSchema";
import Comment from "../models/commentSchema";
const router = express.Router();

export const createPost = async (req: any, res: Response) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }

  try {
    const { title, description } = req.body;
    const imgUrl = req.file?.path;
    const userId = req.user.id;
    const post = new Post({
      title,
      description,
      image: imgUrl,
      user: userId,
    });

    const savePost = await post.save();

    res.json(savePost);
  } catch (error: any) {
    return res
      .status(500)
      .send({ error: error?.message || "Internal Server Error" });
  }
};

export const editPost = async (req: any, res: Response) => {
  const { title, description, image } = req.body;
  try {
    const newPost: any = {};
    if (title) newPost.title = title;
    if (description) newPost.description = description;
    if (image) newPost.image = image;

    let post: any = await Post.findById(req.params.id);
    if (!post) return res.status(404).send("User not found");

    if (post.user.toString() !== req.user.id)
      return res.status(401).send("Not Allowed");

    post = await Post.findByIdAndUpdate(
      req.params.id,
      { $set: newPost },
      { new: true }
    );
    res.json({ post });
  } catch (error: any) {
    return res
      .status(500)
      .send({ error: error?.message || "Internal Server Error" });
  }
};

export const fetchAllPosts = async (req: any, res: Response) => {
  try {
    const posts = await Post.find({ user: req.user.id });
    return res.status(200).json(posts);
  } catch (error) {
    return res.status(500).send({ error: "Internal Server Error" });
  }
};

export const postComment = async (req: Request, res: Response) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }

  try {
    const { text } = req.body;
    const postId = req.params.id;
    const comment = await Comment.create({ postId, text });
    res.json(comment);
  } catch (error: any) {
    return res
      .status(500)
      .send({ error: error?.message || "Internal Server Error" });
  }
};

// Like endpoint
export const likePost = async (req: any, res: Response) => {
  try {
    const postId = req.params.id;

    const userData = await Post.findByIdAndUpdate(
      postId,
      { $push: { likes: req.user?.id } },
      { new: true }
    );

    if (!userData) throw Error("User not found");
    res.status(200).send(userData);
  } catch (error) {
    return res.status(500).send({ error: "Internal Server Error" });
  }
};

//unlike
export const unlikePost = async (req: any, res: Response) => {
  try {
    const postId = req.params.id;

    const userData = await Post.findByIdAndUpdate(
      postId,
      { $push: { unlikes: req.user.id } },
      { new: true }
    );

    if (!userData) throw Error("User not found");
    res.status(200).send(userData);
  } catch (error) {
    return res.status(500).send({ error: "Internal Server Error" });
  }
};
