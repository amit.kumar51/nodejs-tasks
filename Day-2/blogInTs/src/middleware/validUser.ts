import { Response, Request, NextFunction } from "express";
import dotenv from "dotenv";
dotenv.config();
import jwt from "jsonwebtoken";

const SECRET_KEY: any = process.env.SECRETKEY;

const validUser = (req: any, res: Response, next: NextFunction) => {
  const token: any = req.header("auth-token");
  if (!token) res.status(401).send({ error: "Authentication token invalid" });

  try {
    const data: any = jwt.verify(token, SECRET_KEY);
    req.user = data.user;
    next();
  } catch (error) {
    res.status(401).send({ error: "Authentication token invalid" });
  }
};

export default validUser;
